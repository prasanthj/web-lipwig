FROM python:2.7.15-wheezy
RUN pip install short_url
ADD lipwig.py /
ENTRYPOINT ["python", "/lipwig.py", "-w"]
EXPOSE 8888